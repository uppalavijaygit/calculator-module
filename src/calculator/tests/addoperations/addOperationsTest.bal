import ballerina/test;
import ballerina/io;

@test:Config {
    groups: ["g1"]
}
function testAddFunction() {
    io:println("Testing Add operation ::");
    int actual= add(5,10);
    int expected= 15;
    test:assertEquals(actual,expected,msg = "Test Failed to return 15");
}

@test:Config {
    groups: ["test"]
}
function testAddFunction2() {
    io:println("Testing Add operation from Test Group::");
    int actual= add(5,10);
    int expected= 15;
    test:assertEquals(actual,expected,msg = "Test Failed to return 15");
}
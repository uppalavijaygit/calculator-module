import ballerina/test;
import ballerina/io;

@test:Config {
    groups: ["g2"]
}
function testMultiplyFunction() {
    io:println("Testing multiply operation ::");
    int actual= multiply(5,10);
    int expected= 50;
    test:assertEquals(actual,expected,msg = "Test Failed to return 50");
}
